import './Style.scss';
import Button from "./components/Button";
import Modal from "./components/Modal";
import {useState} from "react";

function App() {

    const [showModal1, setShowModal1] = useState(false);
    const [showModal2, setShowModal2] = useState(false);

    const toggleModal1 = () => {
        setShowModal1(!showModal1);
    };

    const toggleModal2 = () => {
        setShowModal2(!showModal2);
    };

    const modalActions1 = (
        <div className="button-container">
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Ok" onClick={toggleModal1}/>
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Cancel" onClick={toggleModal1}/>
        </div>
    );

    const modalActions2 = (
        <div className="button-container">
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Yes" onClick={toggleModal2}/>
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="No" onClick={toggleModal2}/>
        </div>
    );

    return (
        <div className="App">
            <Button backgroundColor="blue" textColor="yellow" text="Open first modal" onClick={toggleModal1}/>
            <Button backgroundColor="yellow" textColor="blue" text="Open second modal" onClick={toggleModal2}/>

            {showModal1  && (<Modal header="Ukraine: Country of Hope and Aspiration" closeButton={true} onClick={toggleModal1} text="Ukraine is an amazing country rich in history and cultural heritage. Its picturesque scenery, hospitable people and national pride make it unique on the world stage." actions={modalActions1}/>)}
            {showModal2  && (<Modal header="Question" closeButton={false} onClick={toggleModal2} text="Have you ever been to Ukraine?" actions={modalActions2}/>)}
        </div>
    );
}

export default App;