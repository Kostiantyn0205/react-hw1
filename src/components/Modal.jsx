import {useEffect} from "react";

const Modal = ({ header, closeButton, onClick, text, actions }) => {
    const clickOutside = (event) => {
        if (!event.target.closest(".modal-container")) {
            onClick();
        }
    };

    useEffect(() => {
        document.addEventListener("mousedown", clickOutside);
    });

    return (
        <div className="modal-overlay">
            <div className="modal-container">
                {closeButton && (<button className="modal-button-close" onClick={onClick}></button>)}
                <h2>{header}</h2>
                <p>{text}</p>
                {actions}
            </div>
        </div>
    );
};

export default Modal;