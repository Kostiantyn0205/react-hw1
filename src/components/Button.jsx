const Button = ({backgroundColor, textColor, text, onClick}) => {
    const buttonStyle = {
        backgroundColor: backgroundColor,
        color: textColor,
    };

    return <button style={buttonStyle} onClick={onClick}>{text}</button>;
}

export default Button;